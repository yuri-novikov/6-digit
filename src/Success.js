import React from 'react'
import { Link } from 'react-router-dom'

function Success() {
  return (
    <div className="container">
      <img
        src="https://media.giphy.com/media/zaqclXyLz3Uoo/giphy.gif"
        alt="success"
        aria-label="success-image"
      />
      <Link className="back-button" to="/">
        Back
      </Link>
    </div>
  )
}

export default Success
