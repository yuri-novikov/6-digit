import React, { useState } from 'react'
import Success from './Success'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import clsx from 'clsx'

const defaultValues = [
  { value: '', name: 1, error: false },
  { value: '', name: 2, error: false },
  { value: '', name: 3, error: false },
  { value: '', name: 4, error: false },
  { value: '', name: 5, error: false },
  { value: '', name: 6, error: false },
]

function App({ history }) {
  const [values, setValues] = useState(defaultValues)
  const [errorMessage, setErrorMessage] = useState(null)

  // commom handler used on each input
  const onChange = (e) => {
    const { value: newValue, name } = e.target
    setErrorMessage(null)
    // here we need to update our state in immutable way
    let needToFocusNext = true
    const newValues = values.map((oldValueObj) => {
      const { name: oldName, value: oldValue } = oldValueObj

      if (+name === oldName && newValue !== oldValue) {
        let computedValue = ''
        if (newValue.length <= 2) {
          // case when we have 1 or 2 chars (2 is to allow users reinput data)
          computedValue =
            newValue.length === 2 ? newValue.replace(oldValue, '') : newValue
        } else {
          computedValue = newValue[0]
        }

        if (computedValue === '') {
          needToFocusNext = false
        }

        return {
          name: +name,
          value: computedValue,
          error: false,
        }
      }
      return oldValueObj
    })
    setValues(newValues)
    if (needToFocusNext) {
      focusNext(name)
    }
  }

  // onPaste handler
  const onPaste = (e) => {
    e.preventDefault()
    setErrorMessage(null)

    const { name } = e.target
    let pasted = e.clipboardData.getData('Text')
    let lastName = null

    const newValues = values.map((oldValue) => {
      // we need to start from input that is receiveing onPaste event
      const firstLetter = pasted[0]
      if (firstLetter) {
        lastName = oldValue.name
      }

      if (oldValue.name >= +name && firstLetter) {
        pasted = pasted.substring(1)
        return { ...oldValue, value: firstLetter }
      }

      return oldValue
    })
    setValues(newValues)
    if (lastName) {
      focusNext(lastName)
    }
  }

  // backspace and delete press handler
  const onKeyDown = (e) => {
    const { name, value } = e.target

    // backspace
    if (e.keyCode === 8) {
      if (+name === 6 && value !== '') {
        const newValues = values.map((oldValue) => {
          return oldValue.name === 6 ? { ...oldValue, value: '' } : oldValue
        })
        setValues(newValues)
        e.preventDefault()
      } else {
        focusNext(name, true)
      }
    }

    // delete
    if (e.keyCode === 46) {
      const newValues = values.map((oldValue) =>
        oldValue.name === +name ? { ...oldValue, value: '' } : oldValue,
      )
      setValues(newValues)
      e.preventDefault()
    }
  }

  // request for code verification
  const onSubmit = async (e) => {
    e.preventDefault()

    // cliend side validation
    const newValues = values.map(({ name, value, error }) => {
      return {
        name,
        value,
        error: !+value.match(/\d/),
      }
    })
    setValues(newValues)

    if (newValues.find((value) => value.error)) {
      return
    }
    const apiUrl =
      process.env.NODE_ENV === 'test' ? 'http://localhost:8081' : ''
    try {
      const json = await fetch(`${apiUrl}/verify`, {
        method: 'POST',
        body: JSON.stringify(values),
        headers: {
          'Content-Type': 'application/json',
        },
      })
      const data = await json.json()
      if (data.passed) {
        setErrorMessage(null)
        history.push('/success')
      } else {
        setErrorMessage(data.message)
      }
    } catch (e) {
      setErrorMessage('Something is wrong. Please, try again later.')
    }
  }

  return (
    <form onSubmit={onSubmit}>
      <div className="container">
        <label className="label">Verification code:</label>
        <div className="input-box">
          {values.map(({ error, name, value }) => (
            <input
              onPaste={onPaste}
              onChange={onChange}
              onKeyDown={onKeyDown}
              key={name}
              className={clsx('input', error && 'error')}
              autoComplete="off"
              type="text"
              aria-label={`digit-input-${name}`}
              maxLength="2"
              name={name}
              value={value}
            />
          ))}
        </div>
        <button className="button" aria-label="digit-submit" type="submit">
          submit
        </button>
        <div className="validation" aria-label="error">
          {errorMessage}
        </div>
      </div>
    </form>
  )
}

// reusable helper to focus to the next element of the form
function focusNext(elName, backwards = false) {
  const name = +elName
  let nextElQuery = ''
  if (backwards) {
    nextElQuery =
      name > 1 ? `input[name="${name - 1}"]` : `input[name="${name}"]`
  } else {
    nextElQuery =
      name >= 1 && name !== 6
        ? `input[name="${name + 1}"]`
        : `input[name="${name}"]`
  }
  const next = document.querySelector(nextElQuery)

  if (next !== null) {
    next.focus()
  }
}

function CodesApp() {
  return (
    <React.StrictMode>
      <Router>
        <Switch>
          <Route path="/success" component={Success} />
          <Route path="/" component={App} />
        </Switch>
      </Router>
    </React.StrictMode>
  )
}

export default CodesApp
