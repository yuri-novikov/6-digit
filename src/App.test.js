import { render, screen, fireEvent, waitFor } from '@testing-library/react'
import App from './App'

beforeEach(() => {
  render(<App />)
})

test('renders elements properly', () => {
  const labelElement = screen.getByText(/Verification code:/i)
  expect(labelElement).toBeInTheDocument()

  const inputElements = screen.getAllByRole('textbox', { type: 'input' })
  expect(inputElements).toHaveLength(6)

  const buttonElement = screen.getByRole('button', { type: 'submit' })
  expect(buttonElement).toBeInTheDocument()
})

test('users should be able to manually enter the digits, next digit should automatically focus', () => {
  const firstInput = screen.getByLabelText('digit-input-1')

  fireEvent.change(firstInput, { target: { value: '1' } })
  expect(firstInput.value).toBe('1')

  const secondInput = screen.getByLabelText('digit-input-2')
  expect(document.activeElement).not.toEqual(firstInput)
  expect(document.activeElement).toEqual(secondInput)

  // from 2nd to 6th
  fireEvent.change(document.activeElement, { target: { value: '2' } })
  fireEvent.change(document.activeElement, { target: { value: '3' } })
  fireEvent.change(document.activeElement, { target: { value: '4' } })
  fireEvent.change(document.activeElement, { target: { value: '5' } })
  fireEvent.change(document.activeElement, { target: { value: '6' } })

  const inputElements = screen.getAllByRole('textbox', { type: 'input' })
  inputElements.forEach((element, i) => {
    expect(element.value).toEqual(`${i + 1}`) // because we mocked inputs with 1,2,3,4,5,6 values
  })

  const lastInput = screen.getByLabelText('digit-input-6')
  expect(lastInput.value).not.toEqual('2')
})

test('users should be able to paste the code from the clipboard', async () => {
  const clipboard = 'abcdef'

  const firstInput = screen.getByLabelText('digit-input-1')

  const pasteEvent = {
    clipboardData: { getData: jest.fn().mockReturnValueOnce(clipboard) },
  }

  fireEvent.paste(firstInput, pasteEvent)

  await waitFor(() => {
    const inputElements = screen.getAllByRole('textbox', { type: 'input' })
    inputElements.forEach((element, i) => {
      expect(element.value).toEqual(clipboard[i])
    })
  })

  const lastInput = screen.getByLabelText('digit-input-6')
  expect(lastInput.value).not.toEqual('e')
})

test('sends correct data for verification and redirects to success page', async () => {
  const firstInput = screen.getByLabelText('digit-input-1')

  fireEvent.change(firstInput, { target: { value: '1' } })
  fireEvent.change(document.activeElement, { target: { value: '2' } })
  fireEvent.change(document.activeElement, { target: { value: '1' } })
  fireEvent.change(document.activeElement, { target: { value: '5' } })
  fireEvent.change(document.activeElement, { target: { value: '6' } })
  fireEvent.change(document.activeElement, { target: { value: '5' } })

  const buttonElement = screen.getByLabelText('digit-submit')
  fireEvent.click(buttonElement)
  await waitFor(
    () => {
      const successImage = screen.getByLabelText('success-image')
      expect(successImage).toBeInTheDocument()
      const backButton = screen.getByRole('link')
      backButton.click()
    },
    { timeout: 1000 },
  )
})

test('only one character per input allowed', () => {
  const thirdInput = screen.getByLabelText('digit-input-3')
  expect(thirdInput).toBeInTheDocument()

  fireEvent.change(thirdInput, { target: { value: '1123' } })
  expect(thirdInput.value).toHaveLength(1)
})

test('it checks server validation with digit 7 as last digit', async () => {
  const firstInput = screen.getByLabelText('digit-input-1')

  fireEvent.change(firstInput, { target: { value: '1' } })
  fireEvent.change(document.activeElement, { target: { value: '1' } })
  fireEvent.change(document.activeElement, { target: { value: '1' } })
  fireEvent.change(document.activeElement, { target: { value: '1' } })
  fireEvent.change(document.activeElement, { target: { value: '1' } })
  fireEvent.change(document.activeElement, { target: { value: '7' } })

  const buttonElement = screen.getByLabelText('digit-submit')
  fireEvent.click(buttonElement)
  await waitFor(
    () => {
      const errorElement = screen.getByLabelText('error')
      expect(errorElement.textContent).toEqual('Verification Error')
    },
    {
      timeout: 1000,
    },
  )
})
