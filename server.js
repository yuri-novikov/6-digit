const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const path = require('path')
const app = express()

app.use(express.static(path.join(__dirname, 'build')))
app.use(cors())

app.use(bodyParser.json())

app.post('/verify', function (req, res) {
  const errorMessage = 'Verification Error'
  try {
    const code = req.body
      .map(({ value }) => value)
      .filter((val) => val)
      .join('')

    // if received code is not 6 digits long
    if (code.length !== 6) {
      throw Error(errorMessage)
    }

    // the last digit is 7
    if (+code.substr(-1) === 7) {
      throw Error(errorMessage)
    }

    return res.json({ passed: true })
  } catch (e) {
    return res.json({ passed: false, message: e.message })
  }
})

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, 'build', 'index.html'))
})

app.listen(process.env.PORT || 8081)
