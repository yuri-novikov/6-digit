### A small application that allows users to input 6-digit codes as a part of user verification flow.

For easy deploy process client app was built and included into the repository

- To run prod server run `yarn server-prod`

- To run dev server run `yarn server-dev`
